"use strict";

let tableBody = document.querySelector("table#liste tbody");
let inputName = document.querySelector("input.name");
let inputAnzahl = document.querySelector("input.anzahl");
let addButton = document.querySelector("button#addItem");

let addItem = function () {

    let itemTd = document.createElement("td");
    itemTd.textContent = inputName.value;

    let countTd = document.createElement("td");
    countTd.textContent = inputAnzahl.value;

    let actionsTd = document.createElement("td");
    actionsTd.append(createDeleteButton(), createButtonUp(), createButtonDown());

    let tableRow = document.createElement("tr");

    tableRow.append(itemTd, countTd, actionsTd);

    tableBody.append(tableRow);

    updateButtonState();
}

addButton.addEventListener("click", addItem);


let actionsTh = document.createElement("th");
actionsTh.textContent = "Actions";
document.querySelector("table#liste thead tr").appendChild(actionsTh);

let allTableRows = tableBody.querySelectorAll("tr");
for (let i = 0; i < allTableRows.length; i++) {
    allTableRows[i].appendChild(document.createElement("td"));

}

let lastCells = document.querySelectorAll("table#liste td:last-child");

for (let i = 0; i < lastCells.length; i++) {
    lastCells[i].appendChild(createDeleteButton());
    lastCells[i].appendChild(createButtonUp());
    lastCells[i].appendChild(createButtonDown());
}

updateButtonState();


function createDeleteButton() {
    let button = document.createElement("button");
    button.textContent = "x";
    button.className = "removeItem";
    button.addEventListener("click", function () {
        this.parentNode.parentNode.remove();
        updateButtonState();
    });
    return button;
}

function createButtonUp() {
    let button = document.createElement("button");
    button.textContent = "↑";
    button.className = "nachOben";
    button.addEventListener("click", function () {
        let currentRow = this.parentNode.parentNode;
        let prevRow = currentRow.previousElementSibling;
        prevRow.before(currentRow);
        updateButtonState();
    });
    return button;
}

function createButtonDown() {
    let button = document.createElement("button");
    button.textContent = "↓";
    button.className = "nachUnten";
    button.addEventListener("click", function () { 
        let currentRow = this.parentNode.parentNode;
        let nextRow = currentRow.nextElementSibling;
        nextRow.after(currentRow);
        updateButtonState();
    });
    return button;
}


function createButton(content, klasse, action) {
    let button = document.createElement("button");
    button.textContent = content;
    button.className = klasse;
    let eventFunction;
    let currentRow = this.parentNode.parentNode;
    switch(action) {
        case "delete":
                eventFunction = function() {
                currentRow.remove();
                updateButtonState();
            };
            break;
        case "down":
            eventFunction = function() {
                currentRow.nextElementSibling.after(currentRow);
                updateButtonState();
            };
            break;
        case "up":
            eventFunction = function() {
                currentRow.previousElementSibling.before(currentRow);
                updateButtonState();
            };
    }
    button.addEventListener("click", eventFunction);
    return button;
}

function updateButtonState() {

    let allTableRows = tableBody.querySelectorAll("tr");
    for(let i = 0; i < allTableRows.length; i++) {
        allTableRows[i].querySelector("button.nachOben").disabled = !allTableRows[i].previousElementSibling;
        allTableRows[i].querySelector("button.nachUnten").disabled = !allTableRows[i].nextElementSibling;
    }
}